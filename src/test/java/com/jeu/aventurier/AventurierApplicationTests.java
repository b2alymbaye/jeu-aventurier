package com.jeu.aventurier;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AventurierApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testSSSSEEEEEENN(){
        // given
        int positionAventurierDepartLigne = 0;
        int positionAventurierDepartColonne = 3;
        String direction = "SSSSEEEEEENN";

        // when
        Jeu jeu = new Jeu();
        Position position = jeu.commencer(positionAventurierDepartColonne, positionAventurierDepartLigne, direction);

        // then
        assertEquals(position.getColonne(), 9);
        assertEquals(position.getLigne(), 2);
    }

    @Test
    void testOONOOOSSO(){        // Fixme : NOK --> Erreur dans l'enoncé ?
        // given
        int positionAventurierDepartLigne = 6;
        int positionAventurierDepartColonne = 9;
        String direction = "OONOOOSSO";

        // when
        Jeu jeu = new Jeu();
        Position position = jeu.commencer(positionAventurierDepartColonne, positionAventurierDepartLigne, direction);

        // then
        assertEquals(position.getColonne(), 7);
        assertEquals(position.getLigne(), 5);
    }
}
