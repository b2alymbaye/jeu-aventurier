package com.jeu.aventurier;

import com.jeu.aventurier.enumeration.Symbole;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Aventurier {
    private String pseudo;
    private Symbole symbole;
    private Position position;

    public Aventurier(String pseudo, Symbole symbole, Position position) {
        this.pseudo = pseudo;
        this.symbole = symbole;
        this.position = position;
    }

    public Symbole getSymbole() {
        return symbole;
    }

    public void setSymbole(Symbole symbole) {
        this.symbole = symbole;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
