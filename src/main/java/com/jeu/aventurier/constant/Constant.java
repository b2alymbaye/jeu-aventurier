package com.jeu.aventurier.constant;

/**
 * Application constants.
 */
public final class Constant {
    // message
    public static final String PAS_DE_PERSONNAGE_SUR_LA_CARTE_MESSAGE = "Pas de personnage sur la carte.";
    public static final String ANONYME = "ANONYME";

    private Constant() {
    }
}