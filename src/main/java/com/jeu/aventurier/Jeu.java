package com.jeu.aventurier;

import com.jeu.aventurier.constant.Constant;
import com.jeu.aventurier.enumeration.Direction;
import com.jeu.aventurier.enumeration.Symbole;

public class Jeu {
    /**
     * Methode d'entrée pour commencer le jeu.
     * @param positionDepartColonne position (colonne) de départ de l'aventurier.
     * @param positionDepartLigne position (ligne) de départ de l'aventurier .
     * @param direction la liste des directions.
     */
    public Position commencer(int positionDepartColonne, int positionDepartLigne, String direction){
        Position aventurierPositionDepart = new Position(positionDepartColonne, positionDepartLigne);
        Aventurier aventurier = new Aventurier(Constant.ANONYME, Symbole.PERSONNAGE, aventurierPositionDepart);

        Carte carte = new Carte(aventurier);

        carte.initialisationStatique();    // chargement de la carte
        carte.afficher();

        carte.initialiserAventurierPosition();
        carte.afficher();
        carte.afficherLocalisationAventurier();

        for (int i = 0; i < direction.length(); i++){
            switch (direction.charAt(i)){
                case 'E':
                    carte.deplacerAventurier(Direction.E);
                    break;
                case 'S':
                    carte.deplacerAventurier(Direction.S);
                    break;
                case 'N':
                    carte.deplacerAventurier(Direction.N);
                    break;
                case 'O':
                    carte.deplacerAventurier(Direction.O);
                    break;
                default:
            }

            carte.afficherLocalisationAventurier();
        }

        carte.afficher();
        carte.afficherLocalisationAventurier();

        return carte.localiserAventurierPosition();
    }
}
