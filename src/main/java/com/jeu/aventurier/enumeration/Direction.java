package com.jeu.aventurier.enumeration;

public enum Direction {
    E("EST"), // deplacement vers la droite
    O("OUEST"), // gauche
    N("NORD"),  // haut
    S("SUD");  // bas

    private String code;

    Direction(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
