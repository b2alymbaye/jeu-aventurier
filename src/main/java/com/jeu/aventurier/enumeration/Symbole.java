package com.jeu.aventurier.enumeration;

public enum Symbole {
    PERSONNAGE(99, "X"),
    ESPACE(0, " "),
    BOIS(-1, "#");

    private int valeur;
    private String code;

    Symbole(int valeur, String code){
        this.valeur = valeur;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public int getValeur() {
        return valeur;
    }
}
