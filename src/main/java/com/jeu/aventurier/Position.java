package com.jeu.aventurier;

public class Position {
    private int colonne;
    private int ligne;

    public Position() {
    }

    public Position(int colonne, int ligne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public int getColonne() {
        return colonne;
    }

    public void setColonne(int colonne) {
        this.colonne = colonne;
    }

    public int getLigne() {
        return ligne;
    }

    public void setLigne(int ligne) {
        this.ligne = ligne;
    }

    public void afficher(){
        System.out.println("Colonne : "+ this.colonne );
        System.out.println("Ligne : "+ this.ligne );
    }
}
