package com.jeu.aventurier;

import com.jeu.aventurier.constant.Constant;
import com.jeu.aventurier.enumeration.Direction;
import com.jeu.aventurier.enumeration.Symbole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Scanner;

public class Carte {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private int nombreColonne;
    private int nombreLigne;
    private int contenu[][];
    private Aventurier aventurier;

//    TODO: Pour la generation dynamique.
//    public Carte(int nombreColonne, int nombreLigne, Aventurier aventurier) {
//        this.nombreColonne = nombreColonne;
//        this.nombreLigne = nombreLigne;
//        this.contenu = new int[nombreColonne][nombreLigne];
//        this.aventurier = aventurier;
//    }

    public Carte(Aventurier aventurier) {
        this.aventurier = aventurier;
    }

    /**
     * Methode permettant le chargement de la carte depuis un fichier .txt et initialisation de la carte.
     */
    public void initialisationStatique() {
        try {
            File myObj = new File("carte.txt");

            if (myObj.exists()) {
                System.out.println("Initialisation de la carte.");
                Scanner myReader = new Scanner(myObj);
                FileReader fileReader = new FileReader(myObj);
                LineNumberReader lineNumberReader = new LineNumberReader(fileReader);

                int nombreLigne = 0;
                int nombreColonne = 0;
                // calcul du nombre de ligne
                while (lineNumberReader.readLine() != null){
                    nombreLigne++;
                }
                lineNumberReader.close();
                setNombreLigne(nombreLigne);

                int ligne = 0;
                while (myReader.hasNextLine()) {
                    String data = "";

                    if (ligne != 0){
                        data = myReader.nextLine();
                    } else {  // on crée le contenu
                        // calcul du nombre de colonne
                        if (myReader.hasNextLine()){
                            data = myReader.nextLine();
                            nombreColonne = data.length();
                            setNombreColonne(nombreColonne);
                        }

                        this.contenu = new int[nombreLigne][nombreColonne];
                    }

                    // intialisation du contenu
                    for (int colonne = 0; colonne < data.length(); colonne++) {
                        this.contenu[ligne][colonne] = getSymboleValeur(data.charAt(colonne));
                    }
                    ligne++;
                }
                myReader.close();
            }
        } catch (IOException e) {
            logger.error("Erreur lors de l'initialisation statique de la carte <ref> {}", e);
        }
    }

//    TODO: Mettre en place la generation de carte aleatoire.
//    public void initialisationDynamique(){
//
//
//    }

    /**
     * Permet d'initialiser la position de l'aventurier.
     */
    public void initialiserAventurierPosition(){
        System.out.println("Initialisation de la position de l'aventurier " + aventurier.getPseudo() + ".");
        this.contenu[aventurier.getPosition().getLigne()][aventurier.getPosition().getColonne()] = aventurier.getSymbole().getValeur();
    }

    /**
     * Permet de mettre a jour la position de l'aventurier.
     */
    public void actualiserAventurierPosition(Position positionAncienne, Position positionNouvelle){
        this.contenu[positionAncienne.getLigne()][positionAncienne.getColonne()] = Symbole.ESPACE.getValeur();
        this.contenu[positionNouvelle.getLigne()][positionNouvelle.getColonne()] = aventurier.getSymbole().getValeur();
    }

    /**
     * Permet de localiser un aventurier sur la carte.
     */
    public Position localiserAventurierPosition(){
        for (int ligne = 0; ligne < this.nombreColonne; ligne++){
            for (int colonne = 0; colonne < this.nombreLigne; colonne++){
                if (contenu[ligne][colonne] == aventurier.getSymbole().getValeur()){
                    return new Position(colonne, ligne);
                }
            }
        }
        System.out.println(Constant.PAS_DE_PERSONNAGE_SUR_LA_CARTE_MESSAGE);
        return new Position(-1, -1);
    }

    /**
     * Permet d'afficher les coordonnées d'un aventurier sur la carte.
     */
    public void afficherLocalisationAventurier(){
        Position position = localiserAventurierPosition();
        System.out.println("Position actuel du personnage : (" + position.getColonne() + ", " + position.getLigne() + ")");
    }

    /**
     * Methode permettant de faire deplacer l'aventurier
     * sur la carte en fonction d'une direction.
     * @param direction
     */
    public void deplacerAventurier(Direction direction){
        Position positionActuelle = localiserAventurierPosition();
        Position positionNouvelle = new Position();

        switch (direction){
            case E: // droite
                if (positionActuelle.getColonne() + 1 <= getNombreColonne() && this.contenu[positionActuelle.getLigne()][positionActuelle.getColonne() + 1] != -1) {
                    positionNouvelle.setColonne(positionActuelle.getColonne() + 1);
                    positionNouvelle.setLigne(positionActuelle.getLigne());
                } else {
                    System.out.println("Deplacement impossible a droite : Coordonnée hors carte.");
                }
                break;
            case S: // bas
                if (positionActuelle.getLigne() + 1 <= getNombreLigne() && this.contenu[positionActuelle.getLigne() + 1][positionActuelle.getColonne()] != -1) {
                    positionNouvelle.setColonne(positionActuelle.getColonne());
                    positionNouvelle.setLigne(positionActuelle.getLigne() + 1);
                } else {
                    System.out.println("Deplacement impossible en bas : Coordonnée hors carte.");
                }
                break;
            case O: // gauche
                if (positionActuelle.getColonne() - 1 <= getNombreColonne() && this.contenu[positionActuelle.getLigne()][positionActuelle.getColonne() - 1] != -1) {
                    positionNouvelle.setColonne(positionActuelle.getColonne() - 1);
                    positionNouvelle.setLigne(positionActuelle.getLigne());
                } else {
                    System.out.println("Deplacement impossible a gauche : Coordonnée hors carte.");
                }
                break;
            case N: // haut
                if (positionActuelle.getLigne() - 1 <= getNombreLigne() && this.contenu[positionActuelle.getLigne() - 1][positionActuelle.getColonne()] != -1) {
                    positionNouvelle.setColonne(positionActuelle.getColonne());
                    positionNouvelle.setLigne(positionActuelle.getLigne() - 1);
                } else {
                    System.out.println("Deplacement impossible vers le haut : Coordonnée hors carte.");
                }
                break;
            default:
        }

        aventurier.setPosition(positionNouvelle);
        actualiserAventurierPosition(positionActuelle, positionNouvelle);
        afficher();
    }

    /**
     * Permet d'afficher la carte.
     */
    public void afficher(){
        for (int ligne = 0; ligne < this.nombreColonne; ligne++){
            for (int colonne = 0; colonne < this.nombreLigne; colonne++){
                System.out.print(getSymboleCode(this.contenu[ligne][colonne]) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Permet d'encoder le symbole sur la carte.
     * @param symboleValeur
     * @return Code symbole
     */
    private String getSymboleCode(int symboleValeur) {
        switch (symboleValeur) {
            case -1:
                return Symbole.BOIS.getCode();
            case 0:
                return Symbole.ESPACE.getCode();
            case 99:
                return Symbole.PERSONNAGE.getCode();
            default:
                return "Pas de code symbole correspondant.";
        }
    }


    /**
     * Permet de decoder le symbole sur la carte.
     * @param symboleCode
     * @return Valeur symbole
     */
    private int getSymboleValeur(char symboleCode) {
        switch (symboleCode) {
            case '#':
                return Symbole.BOIS.getValeur();
            case 'X':
                return Symbole.PERSONNAGE.getValeur();
            case ' ':
            default:
                return Symbole.ESPACE.getValeur(); // on retourne un espace
        }
    }

    public int getNombreColonne() {
        return nombreColonne;
    }

    public void setNombreColonne(int nombreColonne) {
        this.nombreColonne = nombreColonne;
    }

    public int getNombreLigne() {
        return nombreLigne;
    }

    public void setNombreLigne(int nombreLigne) {
        this.nombreLigne = nombreLigne;
    }

    public int[][] getContenu() {
        return contenu;
    }

    public void setContenu(int[][] contenu) {
        this.contenu = contenu;
    }

    public Aventurier getAventurier() {
        return aventurier;
    }

    public void setAventurier(Aventurier aventurier) {
        this.aventurier = aventurier;
    }
}
